const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const cors = require("cors");
const bodyParser = require('body-parser');
require('./database');
const { getRelatedArticles, rateArticle, commentArticle, rateArticleComment } = require('./articleService');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/articles', getRelatedArticles);
app.post('/article/rate', rateArticle);
app.post('/article/comment', commentArticle);
app.post('/article/comment/rate', rateArticleComment);

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
});