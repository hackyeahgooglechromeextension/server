const { Article } = require('./models/article');
const { ArticleGroup } = require('./models/articleGroup');
const axios = require('axios');
const fs = require('fs');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const vulgarity = require('./vulgarity.json');

const BLACK_LIST = ["wykop.pl"];

exports.getRelatedArticles = async (req, res) => {
    const { url } = req.query;

    const articleGroup = await ArticleGroup.findOne({ "relatedArticles.url": { $all: [url] } });

    if (!articleGroup) {
        console.log("!articleGroup")
        const mainArticle = await getArticleData(req.query.url);

        let similarArticlesArray = [];

        try {
            const response = await axios.get(getLink(mainArticle.title));

            const filteredArticles = filterBlackList(response.data.articles);

            const slicedArticles = croppArticles(filteredArticles);

            if (slicedArticles.length > 0) {
                console.log("sliced > 0")
                for (const article of slicedArticles) {
                    fetchArticle(article.url, slicedArticles.length, mainArticle, similarArticlesArray, res);
                }
            } else {
                console.log("sliced = 0")
                const articleGroup = {
                    relatedArticles: [{
                        url: mainArticle.url,
                        articleId: mainArticle.id,
                        fake: mainArticle.fake,
                        favicon: getFavicon(mainArticle.url),
                        title: mainArticle.title
                    }],
                    comments: []
                }

                await ArticleGroup.create(articleGroup);
                await Article.create({
                    title: mainArticle.title,
                    url: mainArticle.url,
                    paragraphs: mainArticle.paragraphs,
                    fake: mainArticle.fake,
                    stats: {
                        true: 0,
                        false: 0
                    },
                    favicon: getFavicon(url)
                });

                return res.status(200).json({ main: mainArticle });
            }
        } catch (err) {
            console.log(err);
        }
    } else {
        console.log("articleGroup")
        const mainArticleDetailed = await Article.findOne({ url });

        const similarArticles = articleGroup.relatedArticles.filter(article => article.url !== url);

        res.status(200).json({
            main: mainArticleDetailed,
            similar: similarArticles,
            comments: articleGroup.comments
        });
    }
}

exports.rateArticle = async (req, res) => {
    const { action, url } = req.body;

    const article = await Article.findOne({ url });

    if (!article) return res.sendStatus(404);

    if (action === "true") {
        article.stats.true += 1;
    } else if (action === "false") {
        article.stats.false += 1;
    }

    res.sendStatus(200);

    article.save();
}

exports.commentArticle = async (req, res) => {
    const { url, comment } = req.body;

    if (comment.length === 0) return res.sendStatus(409);

    // hack for vulgarity validation
    const extendedComment = comment + " ";

    const articleGroup = await ArticleGroup.findOne({ "relatedArticles.url": { $all: [url] } });
    const article = await Article.findOne({ url });

    if (!articleGroup) return res.sendStatus(404);

    if (isVulgar(extendedComment)) {
        console.log("vulgarity")
        return res.sendStatus(409);
    } else {
        articleGroup.comments.push({
            content: extendedComment,
            stats: 0,
            favicon: article.favicon,
            url: article.url,
            title: article.title
        });

        res.sendStatus(200);
        articleGroup.save();
    }
}

exports.rateArticleComment = async (req, res) => {
    const { action, url, id } = req.body;
    const articleGroup = await ArticleGroup.findOne({ "relatedArticles.url": { $all: [url] } });

    if (!articleGroup) return res.sendStatus(404);

    const comment = articleGroup.comments.find(comment => comment.id === id);

    console.log(action)

    if (action === 'increase') {
        comment.stats += 1;
        articleGroup.save();
        return res.sendStatus(200);
    } else if (action === 'decrease') {
        comment.stats -= 1;
        articleGroup.save();
        return res.sendStatus(200);
    }
    res.sendStatus(400);
}

const isVulgar = (comment) => {
    let vulgar = false;

    vulgarity.forEach(word => {
        if (comment.toLowerCase().includes(word + " ") || comment.toLowerCase().includes(" " + word)) {
            console.log(word);
            vulgar = true;
        }
    });

    return vulgar;
}

const addToDatabase = async (articles) => {
    const formattedArticles = [];

    articles.forEach(article => {
        formattedArticles.push({
            title: article.title,
            url: article.url,
            paragraphs: article.paragraphs,
            stats: {
                true: 0,
                false: 0
            },
            fake: false,
            favicon: getFavicon(article.url)
        });
    });

    try {
        const addedArticles = await Article.insertMany(formattedArticles);

        const articlesGroup = {
            comments: [],
            relatedArticles: addedArticles.map(article => {
                return {
                    url: article.url,
                    articleId: article.id,
                    fake: article.fake,
                    title: article.title,
                    favicon: article.favicon
                }
            })
        }

        await ArticleGroup.create(articlesGroup);
    } catch (err) {
        console.log(err);
    }
}

const croppArticles = (articles) => {
    return articles.length > 5 ? articles.slice(0, 5) : articles;
}

const filterBlackList = (articles) => {
    return articles.filter(article => !BLACK_LIST.includes(article.source.name.toLowerCase()));
}

const getLink = (title) => {
    return `https://newsapi.org/v2/everything?q=${encodeURIComponent(title.split(" ").slice(0, 4).join(" "))}&sortBy=publishedAt&apiKey=60e5d5350b4146d8bd19481eaa258906`;
}

const getArticleData = async (url) => {
    try {
        const res = await axios.get(url);

        const dom = new JSDOM(res.data);

        const title = getTitle(dom);

        const paragraphs = getParagraphs(dom);

        return {
            title,
            paragraphs,
            url,
            favicon: getFavicon(url)
        }
    } catch (err) {
        console.log("GetArticle:", err);
    }
}

const getFavicon = (url) => {
    const domain = url.split("/")[2];

    if (domain.split(".")[0] === "www") {
        return domain + "/favicon.ico";
    } else {
        return "www." + domain + "/favicon.ico";
    }
}

const getParagraphs = (dom) => {
    const paragraphs = dom.window.document.querySelectorAll("p");

    const paragraphsArray = [];

    for (const key in paragraphs) {
        if (typeof (paragraphs[key].textContent) !== "undefined" && paragraphs[key].textContent.split(" ").length > 20) {
            paragraphsArray.push(paragraphs[key].textContent.trim().replace(/\s\s+/g, ' '));
        }
    }

    return paragraphsArray;
}

const getTitle = (dom) => {
    const titles = dom.window.document.querySelectorAll("h1");

    const titlesArray = [];

    for (const key in titles) {
        if (typeof (titles[key].textContent) !== "undefined") {
            titlesArray.push(titles[key].textContent);
        }
    }

    let selectedTitle = "";

    for (const title of titlesArray) {
        if (title.trim().length > selectedTitle.length) {
            selectedTitle = title.trim();
        }
    }

    return selectedTitle;
}

const fetchArticle = async (url, articlesAmount, mainArticle, similarArticlesArray, res) => {
    const fetchedArticle = await getArticleData(url);

    similarArticlesArray.push(fetchedArticle);

    console.log(similarArticlesArray.length);

    if (similarArticlesArray.length === articlesAmount) {
        const filteredArticles = similarArticlesArray.filter(article => article ? article : false);

        fs.writeFile("./test.txt", JSON.stringify({ main: mainArticle, similar: filteredArticles }, null, 2), function (err) {
            if (err) return console.log(err);
            console.log("The file was saved!");
        });

        res.status(200).json({ main: mainArticle, similar: filteredArticles });
        addToDatabase([mainArticle, ...filteredArticles]);
    }
}