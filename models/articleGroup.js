const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const relatedArticleSchema = new Schema({
    url: String,
    articleId: String,
    fake: Boolean,
    favicon: String,
    title: String
});

const articleGroupSchema = new Schema({
    relatedArticles: {
        type: [relatedArticleSchema],
        required: true
    },
    comments: [{
        content: String,
        stats: {
            type: Number,
            default: 0
        },
        url: String,
        favicon: String,
        title: String
    }]
});

const ArticleGroup = mongoose.model('ArticleGroup', articleGroupSchema);

exports.ArticleGroup = ArticleGroup;