const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const article = new Schema({
    title: String,
    url: String,
    paragraphs: [String],
    stats: {
        true: {
            type: Number,
            default: 0
        },
        false: {
            type: Number,
            default: 0
        }
    },
    fake: Boolean,
    favicon: String
});

const Article = mongoose.model('Article', article);

exports.Article = Article;