const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.export = (async () => {
    try {
        await mongoose.connect(
            'mongodb+srv://admin:wlaubWxdTWjerr6l@cluster0-yxg73.mongodb.net/extension',
            { useNewUrlParser: true, useUnifiedTopology: true }
        );
        console.log('Database successfully connected');
    } catch (err) {
        console.log(`${err} Could not connect to the database. Exiting now...`);
        process.exit();
    }
})();