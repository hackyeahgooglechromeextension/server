{
  "main": {
    "title": "Kylie Moore-Gilbert named as British-Australian academic held in Iran",
    "paragraphs": [
      "\"We believe that the best chance of securing Kylie's safe return is through diplomatic channels,\" her family said in a statement issued through the Australian government.",
      "On Tuesday the Australian government identified two other Australians - Mark Firkin and Jolie King, who also holds a UK passport - who are also being detained in Iran.",
      "They were blogging their travels in Asia and the Middle East and were reportedly arrested 10 weeks ago near Tehran. Their arrest is not believed to be related to that of Dr Moore-Gilbert.",
      "All three are reportedly being held in Tehran's Evin prison, where British-Iranian woman Nazanin Zaghari-Ratcliffe has been jailed since 2016 on spying charges.",
      "On Thursday Australia's Foreign Minister Marise Payne said the government had been working on securing their release for more than a week.",
      "\"The government have been making efforts to ensure they are being treated fairly, humanely and in custom to international norms,\" she said.",
      "Dr Moore-Gilbert's profile on the University of Melbourne website says she is a lecturer in Islamic Studies who focuses on Arab Gulf states.",
      "While the charges against her have not been disclosed, 10-year terms are routinely given in Iran for spying charges, the UK's Times newspaper said.",
      "The situation comes amid a growing stand-off between the West and Iran - although Ms Payne said the cases of those detained were not related to diplomatic tensions.",
      "Relations between the UK and Iran have also been strained in recent months by a row over the seizure of oil tankers in the Gulf.",
      "Australia also announced in July that it would join the US and the UK in policing the Strait of Hormuz against Iranian threats."
    ],
    "url": "https://www.bbc.com/news/world-middle-east-49700070",
    "favicon": "www.bbc.com/favicon.ico"
  },
  "similar": [
    {
      "title": "Family Releases Name Of Australian Woman Detained In Iran",
      "paragraphs": [
        "One of three Australians recently revealed to be detained in Iran was identified by her family on Saturday as a Melbourne University lecturer.",
        "Academic Kylie Moore-Gilbert, who specialises in Middle Eastern politics with a focus on Gulf states, has been held for a \"number of months\" in Iran on charges that remain unclear.",
        "\"Our family thanks the Government and the University of Melbourne for their ongoing support at this distressing and sensitive time,\" a statement released by Australia's foreign affairs department on behalf of the family said.",
        "Perth-based travel-blogging couple Jolie King and Mark Firkin, who has been documenting their journey from home to Britain on social media, were revealed as two of those arrested.",
        "Moore-Gilbert is the last of the three to be named. Her arrest is unrelated to that of the couple, whose charges also remain unclear.",
        "Australian Foreign Minister Marise Payne last week said Moore-Gilbert has been held for \"a number of months\", while King and Firkin for \"a number of weeks\".",
        "\"The government have been making efforts to ensure they are being treated fairly, humanely and in custom to international norms,\" she told parliament last week.",
        "Payne said diplomacy presented the \"best chance\" for their release, with the foreign affairs department in talks with Iranian counterparts about both cases.",
        "News of the arrests came after Canberra announced it would contribute a frigate and surveillance aircraft to a US-led mission to protect shipping through the Strait of Hormuz, with tensions high in the Gulf region.",
        "The Australian government last week updated its travel advice for Iran to \"reconsider your need to travel\" and \"do not travel\" to areas near the border with Iraq and Afghanistan.",
        "Get Breaking news, live coverage, and Latest News from India and around the world on NDTV.com. Catch all the Live TV action on NDTV 24x7 and NDTV India. Like us on Facebook or follow us on Twitter and Instagram for latest news and live news updates."
      ],
      "url": "https://www.ndtv.com/world-news/family-releases-name-of-australian-woman-detained-in-iran-2100850",
      "favicon": "www.ndtv.com/favicon.ico"
    },
    {
      "title": "Was British-Australian academic arrested because she knew of Tehran's links to Bahrain uprising?",
      "paragraphs": [
        "Cambridge-educated Dr Kylie Moore-Gilbert, who teaches Middle Eastern politics at Melbourne University, has been in solitary confinement at the notorious Evin prison for almost a year.",
        "The academic is believed to have been researching a subject regarded as controversial in Iran – the Islamic Republic’s relationship with the Shia community of Bahrain after a sectarian uprising in 2011.",
        "Iran is accused by its neighbours of instigating and supporting the Shia population of Bahrain to rise against its Sunni government, in a bid to extend its control inside the tiny Arab kingdom.",
        "Dr Moore-Gilbert is feared to have been tried in a secret court, after which she was sentenced to ten years for spying.",
        "The lecturer’s identity emerged only days after that of another British-Australian detainee called Jolie King, and her Australian boyfriend, Mark Firkin, were revealed.",
        "Dr Moore-Gilbert's family have confirmed her identity through a statement released by the Department of Foreign Affairs and Trade in Australia",
        "British-Australian travel bloggers Jolie King, and her Australian boyfriend Mark Firkin, both from Perth, have been held in the same jail without charge for about ten weeks",
        "Evin prison is located in the Evin neighborhood of Tehran, Iran. It is notable as the primary site for the housing of Iran's political prisoners since 1972",
        "The three Australians are being held in Evin prison, a notorious Iranian jail where high-profile political detainees are kept while they are interrogated (pictured a prison guard at Evin prison)",
        "The two travel writers are also at Evin prison, alongside British-Iranian national Nazanin Zaghari-Ratcliffe, a mother of one, who has been in jailed on spying charges since 2016.",
        "Jolie King and her boyfriend Mark Firkin (pictured together) have been held in Tehran's notorious Evin prison without charge for around ten weeks",
        "The University of Melbourne lecturer is believed to have been imprisoned in Iran since October 2018, and held in solitary confinement the entire time",
        "Former inmates of Evin (pictured) have described the frequent use of torture by sadistic guards while witnessing people being hanged in the jail's central courtyard",
        "Editor-in-chief Pouria Zeraati said Dr Moore-Gilbert, Ms King and Mr Firkin may be used as bargaining chips to demand the release of Iranian prisoners in the West.",
        "No comments have so far been submitted. Why not be the first to send us your thoughts, or debate this issue live on our message boards.",
        "We will automatically post your comment and a link to the news story to your Facebook timeline at the same time it is posted on MailOnline. To do this we will link your MailOnline account with your Facebook account. We’ll ask you to confirm this for your first post to Facebook.",
        "You can choose on each post whether you would like it to be posted to Facebook. Your details from Facebook will be used to provide you with tailored content, marketing and ads in line with our Privacy Policy."
      ],
      "url": "https://www.dailymail.co.uk/news/article-7464941/Was-British-Australian-academic-arrested-knew-Tehrans-links-Bahrain-uprising.html",
      "favicon": "www.dailymail.co.uk/favicon.ico"
    },
    {
      "title": "Shape\n    Created with Sketch.\n    \n    \n        \n            \n                \n                    \n                \n            \n        \n    \n\nIn photos: British-flagged tanker seized by Iran",
      "paragraphs": [
        "The Australian Government has named the third person revealed this week to be in detention in prison in Iran as a Melbourne academic who has published work on the 2011 Arab uprisings and on authoritarian governments.",
        "British-Australian Kylie Moore-Gilbert, a Cambridge-educated academic who is now a lecturer in Islamic Studies at Melbourne University, has been in Tehran's notorious Evin prison for almost a year, having reportedly been given a 10-year sentence.",
        "Her case came to light this week along with those of another British-Australian woman, Jolie King, and her Australian boyfriend Mark Firkin, who have been held for the past 10 weeks in an unrelated incident.",
        "From 15p €0.18 $0.18 USD 0.27 a day, more exclusives, analysis and extras.",
        "The University of Melbourne's website lists Dr Moore-Gilbert on its “Find an expert” page as a lecturer at the university's Asia Institute.",
        "It says she “specialises in Middle Eastern politics, with a particular focus on the Arab Gulf states,” and that she had published work on the 2011 Arab uprisings, authoritarian governance, and on the role of new media technologies in political activism.",
        "The Department of Foreign Affairs and Trade on Saturday released a statement from Dr Moore-Gilbert's family, which said: “We have been and continue to be in close contact with the Australian Government.",
        "“Our family thanks the Government and the University of Melbourne for their ongoing support at this distressing and sensitive time. We believe that the best chance of securing Kylie's safe return is through diplomatic channels.",
        "“We will not be making any further comment and would like to request that our privacy - and that of our wider family and friends - is respected at this time.”",
        "Although Iranian authorities have not made public the charges against Dr Moore-Gilbert, 10-year sentences have regularly been dealt in the country to those convicted of spying.",
        "While news of the trio's incarcerations only surfaced this week, Australia's Foreign Minister Marise Payne says she has raised their cases “many times” with her Iranian counterpart Javad Zarif.",
        "And Senator Payne has denied the arrests were politically motivated, although some reports have speculated the trio are being held in hope of giving Iran political leverage on a number of ongoing disputes with the western countries.",
        "“We have no reason to think that these arrests are connected to international concern over Iran's nuclear programme, United Nations sanction enforcement or maritime security concerning the safety of civilian shipping,” Senator Payne said.",
        "News of the three prisoners this week has come amid a downturn in relations between Britain and Iran, sparked by issues including the Royal Marines' seizure near Gibraltar in July of an Iranian oil tanker, the Adrian Darya 1.",
        "Iran responded by seizing British-flagged oil tanker the Stena Impero in what was another chapter in a campaign of interfering with shipping in the Strait of Hormuz.",
        "Relations between Tehran and the west, especially the United States, have also been strained over attempts to restrict Iran's nuclear programme.",
        "Ms King and Mr Firkin, who left their home in Perth, Western Australia, in 2017, had been posting updates on their trip across Asia on social media before being arrested, reportedly for endeavouring to use a drone to take aerial footage.",
        "Evin prison, the main detention centre for Iran's political prisoners, also houses 41-year-old Nazanin Zaghari-Ratcliffe, a British-Iranian mother of one who is midway through a five year sentence on spying charges which began in 2016.",
        "While Iran has not commented publicly on any of the arrests, in April Foreign Minister Zarif proposed swapping Mrs Zaghari-Ratcliffe for Negar Ghodskani, an Iranian woman in jail in the US.",
        "Want to discuss real-world problems, be involved in the most engaging discussions and hear from the journalists? Try Independent Premium free for 1 month.",
        "Independent Premium Comments can be posted by members of our membership scheme, Independent Premium. It allows our most engaged readers to debate the big issues, share their own experiences, discuss real-world solutions, and more. Our journalists will try to respond by joining the threads when they can to create a true meeting of independent Premium. The most insightful comments on all subjects will be published daily in dedicated articles. You can also choose to be emailed when someone replies to your comment.",
        "The existing Open Comments threads will continue to exist for those who do not subscribe to Independent Premium. Due to the sheer scale of this comment community, we are not able to give each post the same level of attention, but we have preserved this area in the interests of open debate. Please continue to respect all commenters and create constructive debates.",
        "Want to bookmark your favourite articles and stories to read or reference later? Try Independent Premium free for 1 month to access this feature."
      ],
      "url": "https://www.independent.co.uk/news/world/middle-east/kylie-moore-gilbert-iran-jail-prison-jolie-king-mark-firkin-a9105396.html",
      "favicon": "www.independent.co.uk/favicon.ico"
    },
    {
      "title": "Third person detained in Iran identified as British-Australian academic Dr Kylie Moore-Gilbert",
      "paragraphs": [
        "The Australian Government has named the third person revealed this week to be in detention in prison in Iran as a Melbourne academic who has published work on the 2011 Arab uprisings.",
        "British-Australian Kylie Moore-Gilbert, a Cambridge-educated academic who is now a lecturer in Islamic Studies at Melbourne University, has been in Tehran’s notorious Evin prison for almost a year, having reportedly been given a 10-year sentence.",
        "Her case came to light this week along with those of another British-Australian woman, Jolie King, and her Australian boyfriend Mark Firkin, who have been held for the past 10 weeks in an unrelated incident.",
        "The University of Melbourne’s website lists Dr Moore-Gilbert on its \"find an expert\" page as a lecturer at the university’s Asia Institute.",
        "It says she \"specialises in Middle Eastern politics, with a particular focus on the Arab Gulf states,\" and she had published work on the 2011 Arab uprisings, authoritarian governance, and on the role of new media technologies in political activism.",
        "The Department of Foreign Affairs and Trade on Saturday released a statement from Dr Moore-Gilbert’s family, which said: \"We have been and continue to be in close contact with the Australian Government.",
        "\"Our family thanks the Government and the University of Melbourne for their ongoing support at this distressing and sensitive time. We believe that the best chance of securing Kylie’s safe return is through diplomatic channels.",
        "\"We will not be making any further comment and would like to request that our privacy - and that of our wider family and friends - is respected at this time.\"",
        "We made it! 🏔 . ‘The Three Passes’, ‘Everest Base Camp’, 28 days, a stack of pretty strenuous kms (and probably too many Snickers) all done and dusted. . This pic was taken on day 22 of the trek at 5360m as we went over the final, ‘easiest’, and definitely most picturesque pass - Renjo La. That little peak in the background, center shot is Everest and this was the final time we were able to take in that amazing backdrop. . Lugging our life around isn’t exactly a new concept for us though throw in some backpacks and a few pretty high and steep mountains and understandably, although very rewarding, it was definitely a shock to the system. Happy to have the packs off now and into temperatures above zero to plan the next destinations. A nice warm beach for Christmas in southern India/Sri Lanka sure sounds good. 🍻🚐🏝 . . . . . . #himalayas #renjo #gokyo #threepasses #trek #hike #everest #trekking #nepal #backpacking #sagarmatha #hiking #travelphotography #mountains #snow #14er #globetrotter #mounteverest #theoutbound #everestbasecamp #ebc #3passestrek #threepassestrek #glacier #nature #travelphotography #roamtheplanet",
        "Although Iranian authorities have not made public the charges against Dr Moore-Gilbert, 10-year sentences have regularly been dealt in the country to those convicted of spying.",
        "While news of the trio’s incarcerations only surfaced this week, Australia’s Foreign Minister Marise Payne says she has raised their cases \"many times\" with her Iranian counterpart Javad Zarif.",
        "News of the three prisoners this week has come amid a downturn in relations between Britain and Iran, sparked by issues including the Royal Marines’ seizure near Gibraltar in July of an Iranian oil tanker, the Adrian Darya 1.",
        "Iran responded by seizing British-flagged oil tanker the Stena Impero in what was another chapter in a campaign of interfering with shipping in the Strait of Hormuz.",
        "Relations between Tehran and the west, especially the United States, have also been strained over attempts to restrict Iran’s nuclear programme.",
        "Ms King and Mr Firkin, who left their home in Perth, Western Australia, in 2017, had been posting updates on their trip across Asia on social media before being arrested, reportedly for endeavouring to use a drone to take aerial footage.",
        "Evin prison, the main detention centre for Iran’s political prisoners, also houses 41-year-old Nazanin Zaghari-Ratcliffe, a British-Iranian mother of one who is midway through a five year sentence on spying charges which began in 2016.",
        "While Iran has not commented publicly on any of the arrests, in April Foreign Minister Zarif proposed swapping Mrs Zaghari-Ratcliffe for Negar Ghodskani, an Iranian woman in jail in the US.",
        "By using this site, you agree we can set and use cookies. For more details of these cookies and how to disable them, see our cookie policy.",
        "© Copyright ITV plc 2019"
      ],
      "url": "https://www.itv.com/news/2019-09-14/third-person-detained-in-iran-named-as-islamic-studies-academic/",
      "favicon": "www.itv.com/favicon.ico"
    },
    {
      "title": "Kylie Moore-Gilbert named as British-Australian academic held in Iran",
      "paragraphs": [
        "\"We believe that the best chance of securing Kylie's safe return is through diplomatic channels,\" her family said in a statement issued through the Australian government.",
        "On Tuesday the Australian government identified two other Australians - Mark Firkin and Jolie King, who also holds a UK passport - who are also being detained in Iran.",
        "They were blogging their travels in Asia and the Middle East and were reportedly arrested 10 weeks ago near Tehran. Their arrest is not believed to be related to that of Dr Moore-Gilbert.",
        "All three are reportedly being held in Tehran's Evin prison, where British-Iranian woman Nazanin Zaghari-Ratcliffe has been jailed since 2016 on spying charges.",
        "On Thursday Australia's Foreign Minister Marise Payne said the government had been working on securing their release for more than a week.",
        "\"The government have been making efforts to ensure they are being treated fairly, humanely and in custom to international norms,\" she said.",
        "Dr Moore-Gilbert's profile on the University of Melbourne website says she is a lecturer in Islamic Studies who focuses on Arab Gulf states.",
        "While the charges against her have not been disclosed, 10-year terms are routinely given in Iran for spying charges, the UK's Times newspaper said.",
        "The situation comes amid a growing stand-off between the West and Iran - although Ms Payne said the cases of those detained were not related to diplomatic tensions.",
        "Relations between the UK and Iran have also been strained in recent months by a row over the seizure of oil tankers in the Gulf.",
        "Australia also announced in July that it would join the US and the UK in policing the Strait of Hormuz against Iranian threats.",
        "He was one of 21 MPs who lost the Tory whip after rebelling in a bid to stop a no-deal Brexit."
      ],
      "url": "https://www.bbc.co.uk/news/world-middle-east-49700070",
      "favicon": "www.bbc.co.uk/favicon.ico"
    }
  ]
}